import './App.css';
import { useState } from 'react' 

function App() {
  const [count, setCount] = useState(Math.floor(Math.random() * 100) + 1)

  function aumenta(number) {
    setCount(Math.floor(Math.random() * 100) + 1)
  }

  return (
    <div className="App">
      <h1>{count}</h1>
      <button onClick={() => aumenta(1)}>Clique Aqui</button>
    </div>
  );
}

export default App;
